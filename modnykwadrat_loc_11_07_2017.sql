-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Lip 2017, 09:02
-- Wersja serwera: 5.7.11
-- Wersja PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `modnykwadrat`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'url', 'Nasza oferta', NULL, NULL, '#oferta', NULL, 0, 1),
(2, NULL, 'menu_top', 'url', 'Kim jesteśmy?', NULL, NULL, '#kim-jestesmy', NULL, 0, 2),
(3, NULL, 'menu_top', 'url', 'Dlaczego warto nas wybrać?', NULL, NULL, '#dlaczego-warto-nas-wybrać', NULL, 0, 3),
(4, NULL, 'menu_top', 'url', 'Skontaktuj się z nami', NULL, NULL, '#kontakt', NULL, 0, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `offer`
--

INSERT INTO `offer` (`id`, `icon`, `title`, `content`, `created_at`) VALUES
(1, 'offer-icon-595f7261d3639.png', 'Hydraulika', 'Współpracujemy również z profesjonalistami \r\nw dziedzinie instalacji hydraulicznych. \r\nW zakres usług wchodzą między innymi: \r\nsystemy ogrzewania podłogowego i sterowania, serwisy kotłów gazowych, instalacje ogrzewania podłogowego i grzejniki, systemy sterowania salus.', '2017-07-07 11:37:05'),
(2, 'offer-icon-595f72d17b797.png', 'Instalacje elektryczne', 'Montaż instalacji elektrycznych wewnętrznych \r\ni zewnętrznych\r\nMontaż opraw oświetleniowych\r\nInstalacje oświetleniowe\r\nInstalacje elektryczne mieszkaniowe\r\nMontaż rozdzielni\r\nBiały montaż', '2017-07-07 11:38:57'),
(3, 'offer-icon-595f7309b172d.png', 'Stolarka', 'Układanie paneli \r\nTarasy z desek drewnianych \r\noraz kompozytowych\r\nMontaż drzwi', '2017-07-07 11:39:53'),
(4, 'offer-icon-595f737499141.png', 'Flizowanie', 'Układanie: gresu, terakoty, mozaiki\r\nObkładanie łazienek, schodów, balkonów\r\nPodłogi z płytek ceramicznych i kamiennych', '2017-07-07 11:41:40'),
(5, 'offer-icon-595f73cd0bf59.png', 'Zabudowy gipsowo-kartonowe', 'Montaż ścian działowych z płyt \r\ngipsowo-kartonowych\r\nMontaż sufitów podwieszanych z płyt gipsowo – kartonowych\r\nAdaptacja poddaszy z płyt gipsowo – \r\nkartonowych', '2017-07-07 11:43:07'),
(6, 'offer-icon-595f73fe297bb.png', 'Malowanie', 'Malowanie: ścian, sufitów, elewacji, drzwi, okien, \r\nelementów metalowych, lakierowanie drewna\r\nPrzygotowanie powierzchni do prac malarskich\r\nTapetowanie: ścian, sufitów\r\nWykonywanie gładzi gipsowych', '2017-07-07 11:43:58'),
(7, 'offer-icon-595f743ef1852.png', 'Projektowanie', 'Projektowanie architektury wnętrz. \r\nTworzymy dla klientów indywidualnych jak i większych inwestorów. Dbamy o każdy detal ,od koncepcji aż do realizacji. Możesz zobaczyć swoje wnętrze przed rozpoczęciem prac budowlanych w formie wizualizacji 3D w jakim chcesz widoku. Nasz fachowiec doradzi Ci jak zestawić ze sobą materiały budowlane oraz całe wyposażenie Twojego wymarzonego wnętrza.', '2017-07-07 11:45:02');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `section`
--

INSERT INTO `section` (`id`, `title`, `title2`, `content`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Kim jesteśmy?', 'Wnętrze to nasza pasja', '<p>\r\n	Zajmujemy się kompleksowym wykończeniem wnętrz - mieszkań, dom&oacute;w, powierzchni biurowych i lokali usługowych. Posiadamy doświadczenie i profesjonalne podejście do zlecenia. W zakres naszych usług wchodzą generalne remonty, wykończenia ze stanu surowego jak i projektowanie wnętrz. Do każdego klienta podchodzimy indywidualnie. Naszym priorytetem jest &nbsp;Państwa zadowolenie poprzez fachowe, szybkie i solidne wykonanie rob&oacute;t. Pracujemy szybko sprawie &nbsp;i czysto , pozostawiając po sobie zadowolenie klienta. Wnętrza to nasza pasja.</p>', 'section-image-595f8dde99aaf.jpeg', '2017-07-07 13:23:35', '2017-07-10 13:12:07'),
(2, 'Dlaczego warto nas wybrać?', 'Fachowe i solidne  wykonanie robót', '<p>\r\n	Stworzymy miejsce, w kt&oacute;rym będziesz czuł się komfortowo. Nasze projekty będą odzwierciedlać Tw&oacute;j charakter oraz spełniać oczekiwania. Lubimy wyzwania i nieszablonowe rozwiązania. Wykończenie wnętrz to nasza pasja! Sprawdź ofertę i dołącz do grona ludzi zadowolonych ze swojego wnętrza.</p>', 'section-image-595f8df1606d3.jpeg', '2017-07-07 13:31:29', '2017-07-10 15:44:34');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu "description"', 'Instalka CMS Lemonade Studio'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu "title"', 'Instalka'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'oleh@lemonadestudio.pl'),
(5, 'google_recaptcha_secret_key', 'Google reCAPTCHA - secret key', NULL),
(6, 'google_recaptcha_site_key', 'Google reCAPTCHA - site key', NULL),
(7, 'label_top_title', 'Etykieta - Wykończenie  i projektowanie wnętrz', 'Wykończenie \r\ni projektowanie wnętrz'),
(8, 'offer_help_text', 'Oferta - Pomoc', 'Jeżeli czegoś nie znalazłeś w ofercie \r\nto skontaktuj się z nami, \r\nbyć może jesteśmy w stanie Ci pomóc.'),
(9, 'label_adress', 'Kontakt - Adres', 'Matysówka 180b\r\n35-300 Rzeszów'),
(10, 'label_phone', 'Kontakt - Telefon', '+48 728 358 810'),
(11, 'label_email', 'Kontakt - E-Mail', 'biuro@modnykwadrat.pl'),
(12, 'label_copyrights', 'Copyrights', '©2017 Wszelkie prawa zastrzeżone przez Modny Kwadrat');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL,
  `photo_tablet_width` int(11) NOT NULL,
  `photo_tablet_height` int(11) NOT NULL,
  `photo_mobile_width` int(11) NOT NULL,
  `photo_mobile_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `slider`
--

INSERT INTO `slider` (`id`, `label`, `photo_width`, `photo_height`, `photo_tablet_width`, `photo_tablet_height`, `photo_mobile_width`, `photo_mobile_height`) VALUES
(1, 'Slider', 1140, 629, 700, 629, 380, 366);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `slider_photo`
--

INSERT INTO `slider_photo` (`id`, `slider_id`, `filename`, `arrangement`, `title`, `link`) VALUES
(1, 1, 'slider-image-595e2f9c10b72.jpg', 3, 'Urządź się po swojemu i z klasą', NULL),
(2, 1, 'slider-image-595e2f9d08956.jpg', 2, 'Urządź się po swojemu i z klasą', NULL),
(3, 1, 'slider-image-595e2f9dc5dce.jpg', 1, 'Urządź się po swojemu i z klasą', '#oferta');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH","ROLE_USER"]', '2015-07-31 13:46:34', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
