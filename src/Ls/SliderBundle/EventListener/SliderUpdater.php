<?php

namespace Ls\SliderBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\SliderBundle\Entity\SliderPhoto;

class SliderUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (null === $entity->getArrangement()) {
                $slider = $entity->getSlider();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsSliderBundle:SliderPhoto', 'c')
                    ->where('c.slider = :slider')
                    ->setParameter('slider', $slider)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (null === $entity->getArrangement()) {
                $slider = $entity->getSlider();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsSliderBundle:SliderPhoto', 'c')
                    ->where('c.slider = :slider')
                    ->setParameter('slider', $slider)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $slider = $entity->getSlider();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsSliderBundle:SliderPhoto', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.slider = :slider')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('slider', $slider)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsSliderBundle:SliderPhoto';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto();
        }
    }
}