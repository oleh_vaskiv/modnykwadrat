<?php

namespace Ls\SliderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller {
    protected $containerBuilder;

    public function sectionAction($sliderLabel) {
        $em = $this->getDoctrine()->getManager();
        
        $slider = $em->createQueryBuilder()
            ->select('e')
            ->from('LsSliderBundle:Slider', 'e')
            ->where('e.label = :label')
            ->setParameter(':label', $sliderLabel)
            ->getQuery()
            ->getOneOrNullResult();
        
        return $this->render('LsSliderBundle:Front:section.html.twig', array(
            'slider' => $slider
        ));
    }
}
