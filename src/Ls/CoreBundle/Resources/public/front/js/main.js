$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 1) {
       $('#scrolled').addClass('fixed');
    } else {
        $('#scrolled').removeClass('fixed');
   } 
});

$(document).on('click', '.mobile-menu-btn', function () {
    if (!$('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeIn(250);
    }
});

$(document).on('click', '.hide-menu', function () {
    if ($('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeOut(250);
    }
});

//animate move on click menu
$('.nav').on('click', 'a', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        $('.menu-mobile').fadeOut(250);
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

$('.slide .content').on('click', 'a', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        $('.menu-mobile').fadeOut(250);
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

function checkLink(val) {
    var re = /#(.*)/;
    if (re.test(val) && val.length > 1) {
        return true;
    }

    return false;
}

function animateMenu(id) {
    var body = $("html, body");
    
    var top = $(id).offset().top;
    
    var screenWidth = screen.width;
    if (id === "#oferta" && screenWidth > 1160) {
        top = top - 80;
    } else if(id === "#oferta" && screenWidth < 721) {
        top = top + 80;
    }

    body.stop(true, true).animate({scrollTop: top}, 1000, 'swing');
}

var slider= $('.bxslider').bxSlider({
    auto: true,
    speed: 1000,
    slideSelector: '.slide',
    mode: 'fade',
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".bxslider").css("visibility", "visible");
    }
});

/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
  localStorage.cache = idx;
  // Reloads slider, 
  ///goes back to the slide it was on before resize,
  ///removes the stored index.
  function inner(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
  }
}

// Waliduje formularz kontaktowy
function sendContact(path, send_data) {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 !== undefined) {
            _qtip2.destroy(true);
        }
    });

    var send = true;
    var data = {
        name: $('#form_contact_name').val(),
        email: $('#form_contact_email').val(),
        message: $('#form_contact_content').val()
    };

    if (data.name.length === 0) {
        send = false;
        showError('#form_contact_name', 'Wypełnij pole.');
    }
    
    if (data.email.length === 0) {
        send = false;
        showError('#form_contact_email', 'Wypełnij pole.');
    } else if (data.email.length !== 0) {
        if (!checkEmail(data.email)) {
            send = false;
            showError('#form_contact_email', 'Adres e-mail jest niepoprawny');
        }
    }

    if (data.message.length === 0) {
        send = false;
        showError('#form_contact_content', 'Wypełnij pole.');
    }

    if (send) {
        $.ajax({
            type: 'post',
            url: path,
            data: send_data,
            success: function (response) {
                if (response === 'OK') {
                    $('#form_contact_name').val('');
                    $('#form_contact_email').val('');
                    $('#form_contact_content').val('');
                    
                        $('.form-horizontal .message').show().html('<p class="green">Twoja wiadomość została wysłana.</p>')
                        $('.form-horizontal .message').delay(7000).fadeOut();
                } else {
                    $('.form-horizontal .message').show().html('<p class="red">Wystąpił błąd podczas wysyłania wiadomości.</p>')
                    $('.form-horizontal .message').delay(7000).fadeOut();
                }
            }
        });
    }

    return false;
}

function showError(field, error) {
    $(field).qtip({
        content: {
            title: null,
            text: error
        },
        position: {
            my: 'bottom left',
            at: 'top right',
            adjust: {
                x: -30
            }
        },
        show: {
            ready: true,
            event: false
        },
        hide: {
            event: 'click focus unfocus'
        },
        style: {
            classes: 'qtip-red qtip-rounded qtip-shadow'
        }
    });
}

function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}