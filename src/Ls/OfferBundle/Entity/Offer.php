<?php

namespace Ls\OfferBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Offer
 * @ORM\Table(name="offer")
 * @ORM\Entity
 */
class Offer
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    protected $file;

    protected $iconWidth = 55;
    protected $iconHeight = 55;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Offer
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set content
     *
     * @param string $content
     * @return Offer
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Offer
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getThumbSize()
    {
        $size = array();
        $size['width'] = $this->iconWidth;
        $size['height'] = $this->iconHeight;

        return $size;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->icon)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'icon':
                    $sThumbName = Tools::thumbName($this->icon, '_i');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->icon)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'icon':
                    $sThumbName = Tools::thumbName($this->icon, '_i');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize($type)
    {
        $temp = getimagesize($this->getPhotoAbsolutePath($type));
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto($type)
    {
        if ($type == 'icon') {
            $filename = $this->getPhotoAbsolutePath("icon");
            $filename_i = Tools::thumbName($filename, '_i');
            
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_i)) {
                @unlink($filename_i);
            }
        }
    }

    public function getPhotoAbsolutePath($type)
    {
        if (empty($this->icon)) {
            return null;
        } else {
            switch ($type) {
                case 'icon':
                    return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon;
            }
            return null;
        }
    }

    public function getPhotoWebPath($type)
    {
        if (empty($this->icon)) {
            return null;
        } else {
            switch ($type) {
                case 'icon':
                    return DIRECTORY_SEPARATOR .$this->getUploadDir() . DIRECTORY_SEPARATOR . $this->icon;
            }
        }
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/offer';
    }

    public function uploadIcon()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getIcon();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createIconThumbs();

        unset($this->file);
    }

    public function createIconThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath("icon")) {
            $sFileName = $this->getIcon();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameL = Tools::thumbName($sSourceName, '_i');
            $aThumbSizeL = $this->getThumbSize();
            $thumb->adaptiveResize($aThumbSizeL['width'] + 2, $aThumbSizeL['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeL['width'], $aThumbSizeL['height']);
            $thumb->save($sThumbNameL);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        if ($type == 'icon') {
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getIcon();
        }
        
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
    
    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }
}