<?php

namespace Ls\OfferBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Offer controller.
 *
 */
class FrontController extends Controller {
    /**
     * section on index page
     */
    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->orderBy('a.created_at', 'desc')
            ->getQuery()
            ->getResult();
        
        return $this->render('LsOfferBundle:Front:section.html.twig', array(
            'entities' => $entities,
        ));
    }
}
