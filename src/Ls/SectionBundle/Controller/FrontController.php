<?php

namespace Ls\SectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Section controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists Section entity on index page.
     *
     */
    public function sectionAction($id, $type) {
        $em = $this->getDoctrine()->getManager();


        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        if ($type == 1) {
            return $this->render('LsSectionBundle:Front:section1.html.twig', array(
                'entity' => $entity,
            ));
        } else {
            return $this->render('LsSectionBundle:Front:section2.html.twig', array(
                'entity' => $entity,
            ));
        }
    }
}
