<?php

namespace Ls\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Imię i nazwisko',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Imię i nazwisko'
                ),
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('email', null, array(
                'label' => 'E-Mail',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'E-Mail'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
            )
        );
        $builder->add('content', TextareaType::class, array(
                'label' => 'Treść wiadomości',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Treść wiadomości'
                ),
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );

        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact';
    }
}
